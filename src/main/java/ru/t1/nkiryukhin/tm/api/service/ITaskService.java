package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task) throws AbstractEntityNotFoundException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    void clear();

    Task create(String name, String description, Status status) throws AbstractException;

    Task create(String name, String description) throws AbstractException;

    Task create(String name) throws AbstractFieldException;

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id) throws AbstractFieldException;

    Task findOneByIndex(Integer index) throws AbstractFieldException;

    void remove(Task task) throws AbstractEntityNotFoundException;

    Task removeById(String id) throws AbstractFieldException;

    Task removeByIndex(Integer index) throws AbstractFieldException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

}
