package ru.t1.nkiryukhin.tm.api.controller;

import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.*;

public interface ITaskController {

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void clearTasks();

    void completeTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void createTask() throws AbstractException;

    void showTasks();

    void removeTaskById() throws AbstractFieldException;

    void removeTaskByIndex() throws AbstractFieldException;

    void showTaskById() throws AbstractFieldException;

    void showTaskByIndex() throws AbstractFieldException;

    void showTaskByProjectId();

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

}
